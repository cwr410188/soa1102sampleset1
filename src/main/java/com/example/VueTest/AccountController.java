/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.VueTest;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author lendle
 */
@RestController
@CrossOrigin(origins="*")
public class AccountController {
    private List<Account> accounts=new ArrayList<>(List.of(
            new Account("id1", "password1"),
            new Account("id2", "password2"),
            new Account("id3", "password3")
    ));
    @GetMapping("/")
    public ModelAndView indexAction(){
        return new ModelAndView("index");
    }
    @PostMapping("/api/account")
    public void addAccount(@RequestBody Account account){
        accounts.add(account);
    }
    @GetMapping("/api/accounts")
    public List<Account> getAccounts(){
        return accounts;
    }
}
